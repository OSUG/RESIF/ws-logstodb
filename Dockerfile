FROM python:3.9 AS build

RUN apt-get update && apt-get install -y python3-dev libpq-dev build-essential
RUN pip install psycopg2 regex
COPY requirements.txt /
RUN pip3 install --no-cache-dir -r /requirements.txt

FROM python:3.9-slim
RUN apt-get update && apt-get install -y libpq5
RUN pip3 install --no-cache-dir gunicorn
COPY --from=build /usr/local/lib/python3.9/site-packages/ /usr/local/lib/python3.9/site-packages
COPY ws-logstodb/* /app/
WORKDIR /app
ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8000"]
CMD ["webapp"]
