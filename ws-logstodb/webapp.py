from ringserverstats import ringserverstats
from rsyncstats import rsyncstats
from flask import Flask, Response, request, has_request_context
from flask.logging import default_handler
import logging
import os
import sys

application = Flask(__name__)
application.config['DATABASE_URI'] = os.environ.get('DATABASE_URI')
if not application.config['DATABASE_URI']:
    print("DATABASE_URI environment variable required.")
    sys.exit(0)

@application.route("/rsync", methods=['POST'])
def register_rsync_log():
    application.logger.info("Received rsync logs")
    data = request.get_data().decode('utf-8')
    events = rsyncstats.parse_log(data)
    application.logger.info("Parsed %d events. Registering to database",len(events))
    registered_items = rsyncstats.register_events(events, application.config['DATABASE_URI'])
    application.logger.info("Registered %d events", registered_items)
    return Response("Registered %d/%d events"%(registered_items, len(events)))

@application.route("/ringserver", methods=['POST'])
def register_ringserver_log():
    application.logger.info("Received ringserver logs")
    data = request.get_data().decode('utf-8')
    events = ringserverstats.parse_ringserver_log(data)
    application.logger.info("Parsed %d events. Registering to database",len(events))
    registered_items = ringserverstats.register_events(events, application.config['DATABASE_URI'])
    application.logger.info("Registered %d events", registered_items)
    return Response("Registered %d/%d events"%(registered_items, len(events)))

@application.route("/", methods=['GET'])
def about():
    return Response("""
- POST /ringserver Ringserverstats version %s parses ringserver daily txlog messages
- POST /rsync rsyncstats version %s parses rsync logs from rsync server
    """%(ringserverstats.__version__, rsyncstats.__version__))

if __name__ == "__main__":
    application.run(host='0.0.0.0')
