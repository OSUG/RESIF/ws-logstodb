# Webservice to register log files from various sources

Installation :

pipenv install


## Usage

Need to provide a postgresql URI as environment variable.

    DATABASE_URI=postgresql://user@host:port/database

We recommend not to provide password in the DATABASE_URI but provide it in another way :

  * .pgpass file
  * PG_PASS environment var

## Run

Basic run for developpers : 

  PG_PASS=PLOP DATABASE_URI=postgres://resifstats@resif-pgdev.u-ga.fr:5433/resifstats FLASK_ENV=development RUNMODE=test FLASK_APP=webapp.py python webapp.py

In production, use de Docker image provided in the gitlab registry.


